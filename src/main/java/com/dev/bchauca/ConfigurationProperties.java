package com.dev.bchauca;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "file:///bchauca-properties/config.properties")
public class ConfigurationProperties {
	
	@Value("${product.image.URL}")
	private String productImageURL;

	public String getProductImageURL() {
		return productImageURL;
	}

	public void setProductImageURL(String productImageURL) {
		this.productImageURL = productImageURL;
	}
	
	
}
