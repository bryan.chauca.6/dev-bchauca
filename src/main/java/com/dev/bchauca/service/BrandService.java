package com.dev.bchauca.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.bchauca.model.BusinessException;
import com.dev.bchauca.model.Brand;
import com.dev.bchauca.repository.IBrandDAO;

@Service
public class BrandService {
	
	@Autowired
	private IBrandDAO brandDAO;
	
	public void Insert(Brand brand) throws BusinessException {
		brandDAO.save(brand);
	}
}
