package com.dev.bchauca.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.bchauca.model.BusinessException;
import com.dev.bchauca.model.Category;
import com.dev.bchauca.repository.ICategoryDAO;

@Service
public class CategoryService {

	@Autowired
	private ICategoryDAO categoryDAO;
	
	public void Insert(Category category) throws BusinessException {
		categoryDAO.save(category);
	}
}
