package com.dev.bchauca.service;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.bchauca.model.BusinessException;
import com.dev.bchauca.model.Product;
import com.dev.bchauca.repository.IProductDAO;

@Service
public class ProductService  {
	
	@Autowired
	private IProductDAO productoDAO;
	
	@Autowired
	ProductImageService productImageService;
	
	public void Insert(Product product) throws BusinessException {
		productoDAO.save(product);
	}
	
	public void Update(Product product) throws BusinessException {
		productoDAO.save(product);
	}
	
	@Cacheable(value = "products")
	public Product GetById(long id) throws BusinessException {
		Product product = productoDAO.findById(id).orElse(null);
		
		if(product != null) {
			product.setImageUrls(productImageService.getProductImages(id).getUrls());
		}else {
			throw new BusinessException("No se encontró el producto de id: " + String.valueOf(id));
		}
		
		return product;
	}
	
	public List<Product> GetAll() throws BusinessException {
		return productoDAO.findAll();
	}
}
