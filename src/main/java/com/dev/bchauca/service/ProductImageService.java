package com.dev.bchauca.service;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.dev.bchauca.ConfigurationProperties;
import com.dev.bchauca.model.ProductImagesREST;

@Service
public class ProductImageService {

	private ConfigurationProperties configurationProperties;
	
	private final RestTemplate restTemplate;

    public ProductImageService(RestTemplateBuilder restTemplateBuilder, ConfigurationProperties configurationProperties) {
        this.restTemplate = restTemplateBuilder.build();
        this.configurationProperties = configurationProperties;
    }

    public ProductImagesREST getProductImages(long productId) {
        String url = configurationProperties.getProductImageURL() + String.valueOf(productId);
        return this.restTemplate.getForObject(url, ProductImagesREST.class);
    }
}
