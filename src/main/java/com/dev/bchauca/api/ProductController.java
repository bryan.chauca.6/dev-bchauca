package com.dev.bchauca.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dev.bchauca.model.Brand;
import com.dev.bchauca.model.BusinessException;
import com.dev.bchauca.model.Category;
import com.dev.bchauca.model.Product;
import com.dev.bchauca.model.ResponseApi;
import com.dev.bchauca.service.BrandService;
import com.dev.bchauca.service.CategoryService;
import com.dev.bchauca.service.ProductImageService;
import com.dev.bchauca.service.ProductService;

@RestController
@RequestMapping("products")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET, RequestMethod.PUT,RequestMethod.POST})
public class ProductController {

	@Autowired
	ProductService productService;
	
	@Autowired
	BrandService brandService;
	
	@Autowired
	CategoryService categoryService;
	
	@GetMapping("")
	public ResponseEntity<?> getAll() {
		
		ResponseApi response = new ResponseApi();
		
		try {
			
			List<Product> products = productService.GetAll();
			
			if(products.isEmpty()) {
								
				Category category = new Category();
				category.setName("Laptop");
				categoryService.Insert(category);
				
				Brand brandHP = new Brand();
				brandHP.setName("HP");
				brandService.Insert(brandHP);
				
				Brand brandASUS = new Brand();
				brandASUS.setName("ASUS");
				brandService.Insert(brandASUS);
				
				Product product;
				
				product = new Product();
				product.setName("15-dw1085la");
				product.setDescription("Contenido de 15-dw1085la");
				product.setBrand(brandHP);			
				productService.Insert(product);
				
				product = new Product();
				product.setName("15-dy2050la");
				product.setDescription("Contenido de 15-dy2050la");
				product.setBrand(brandHP);
				productService.Insert(product);
				
				product = new Product();
				product.setName("Zenbook 13 OLED UX325EA");
				product.setDescription("Contenido de Zenbook 13 OLED UX325EA");
				product.setBrand(brandASUS);
				productService.Insert(product);
				
				product = new Product();
				product.setName("TUF DASH F15 FX516PC");
				product.setDescription("Contenido de TUF DASH F15 FX516PC");
				product.setBrand(brandASUS);
				productService.Insert(product);
				response.setData(productService.GetAll());
			}else {
				response.setData(products);
			}
			
			response.setStatus(true);
			
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		}catch(BusinessException ex) {
			
			response.setData(null);
			response.setStatus(false);
			response.setMessage(ex.getErrorMessage());
			
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getById(@PathVariable(value = "id") String id) {
		
		ResponseApi response = new ResponseApi();
		
		try {
			Long productId = Long.parseLong(id);
			Product product = productService.GetById(productId);
			response.setData(product);
			response.setStatus(true);
			
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		}catch(BusinessException ex) {
			
			response.setData(null);
			response.setStatus(false);
			response.setMessage(ex.getErrorMessage());
			
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("")
	public ResponseEntity<?> insert(@RequestBody Product product) {
		
		ResponseApi response = new ResponseApi();
		
		try {
			
			productService.Insert(product);
			
			response.setStatus(true);
			
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		}catch(BusinessException ex) {
			
			response.setData(null);
			response.setStatus(false);
			response.setMessage(ex.getErrorMessage());
			
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}		
	}
	
	@PutMapping("")
	public ResponseEntity<?> update(@RequestBody Product product) {
		
		ResponseApi response = new ResponseApi();
		
		try {
			productService.Update(product);
			
			response.setStatus(true);
			
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		}catch(BusinessException ex) {
			
			response.setData(null);
			response.setStatus(false);
			response.setMessage(ex.getErrorMessage());
			
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}		
	}
}
