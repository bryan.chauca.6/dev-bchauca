package com.dev.bchauca.model;

import java.util.List;

public class ProductImagesREST {
	
	private List<String> urls;

	public List<String> getUrls() {
		return urls;
	}

	public void setUrls(List<String> urls) {
		this.urls = urls;
	}
	
	
}
