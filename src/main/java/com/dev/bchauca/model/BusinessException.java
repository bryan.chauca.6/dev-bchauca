package com.dev.bchauca.model;

public class BusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String errorMessage;
	
	public BusinessException(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public BusinessException(String errorMessage, Throwable e) {
		super(errorMessage, e);
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
