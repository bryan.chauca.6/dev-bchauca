package com.dev.bchauca.model;

import java.io.Serializable;

public class ResponseApi implements Serializable {
	
	private static final long serialVersionUID = -4494122410442561350L;
	
	private boolean status;
	private String message;
	private Object data; 
	
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
