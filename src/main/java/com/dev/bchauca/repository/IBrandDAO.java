package com.dev.bchauca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dev.bchauca.model.Brand;

public interface IBrandDAO extends JpaRepository<Brand, Long> {

}
