package com.dev.bchauca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dev.bchauca.model.Category;

public interface ICategoryDAO extends JpaRepository<Category, Long> {

}
