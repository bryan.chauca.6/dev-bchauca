package com.dev.bchauca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dev.bchauca.model.Product;

public interface IProductDAO extends JpaRepository<Product, Long> {

}
